﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Smallest_Multiple
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnClick_Click(object sender, RoutedEventArgs e)
        {
            int limit, num = 1, divisable = 0;
            Boolean exit = false;

            do
            {
                num++;
                divisable = 0;

                for (limit = 1; limit <= 20; limit++)
                {
                    if (num % limit == 0)
                    {
                        divisable++;
                    }
                    else
                    {
                        limit = 20;
                    }
                }

                if(divisable == 20)
                {
                    exit = true;
                }
            }
            while (exit != true);

            lblAnswer.Content = num;

        }
    }
}
